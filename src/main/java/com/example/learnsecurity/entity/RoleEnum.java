package com.example.learnsecurity.entity;

public enum RoleEnum {

    ROLE_USER,
    ROLE_ADMIN;
}
