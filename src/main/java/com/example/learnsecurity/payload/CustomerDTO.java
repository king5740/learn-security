package com.example.learnsecurity.payload;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDTO {
    @NotNull(message = "fullName must not be null")
    private String fullName;

    @NotNull(message = "phoneNumber must not be null")
    private String phoneNumber;

    @NotNull(message = "address must not be null")
    private String address;
}
