package com.example.learnsecurity.contoller;

import com.example.learnsecurity.entity.User;
import com.example.learnsecurity.payload.ApiResponse;
import com.example.learnsecurity.payload.CustomerDTO;
import com.example.learnsecurity.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
public class Controller {
    private final CustomerService customerService;


    @GetMapping
    public List<User> getCustomers() {
        return customerService.getCustomers();
    }

    @GetMapping("/{id}")
    public User getCustomer(@PathVariable Integer id) {
        return customerService.getCustomer(id);
    }

    @PostMapping
    public ApiResponse addCustomer(@Valid @RequestBody CustomerDTO customer) {
        return customerService.addCustomer(customer);
    }

    @DeleteMapping("/{id}")
    public ApiResponse deleteCustomer(@PathVariable Integer id) {
        return customerService.deleteCustomer(id);
    }

    @PutMapping("/{id}")
    public ApiResponse editCustomer(@PathVariable Integer id, @RequestBody CustomerDTO customerDTO) {
        return customerService.editCustomer(id, customerDTO);
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
