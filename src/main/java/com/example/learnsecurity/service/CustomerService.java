package com.example.learnsecurity.service;

import com.example.learnsecurity.entity.User;
import com.example.learnsecurity.payload.ApiResponse;
import com.example.learnsecurity.payload.CustomerDTO;
import com.example.learnsecurity.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public List<User> getCustomers() {
        return customerRepository.findAll();
    }

    public User getCustomer(Integer id) {
        Optional<User> byId = customerRepository.findById(id);
        return byId.get();
    }

    public ApiResponse addCustomer(CustomerDTO customer) {
        if (customerRepository.existsByPhoneNumber(customer.getPhoneNumber())) {
            return new ApiResponse("customer exists already", false);
        }
        User newUser = new User(customer.getFullName(), customer.getPhoneNumber(), customer.getAddress());

        customerRepository.save(newUser);

        return new ApiResponse("saved successfully", true);
    }

    public ApiResponse editCustomer(Integer id, CustomerDTO customerDTO) {
        if (customerRepository.existsByPhoneNumberAndIdNot(customerDTO.getPhoneNumber(), id)) {
            return new ApiResponse("customer already exsits", false);
        }

        Optional<User> byId = customerRepository.findById(id);
        if (byId.isEmpty())
            return new ApiResponse("customer does not exists", false);

        User user = byId.get();
        user.setAddress(customerDTO.getAddress());
        user.setFullName(user.getFullName());
        user.setPhoneNumber(customerDTO.getPhoneNumber());

        customerRepository.save(user);

        return new ApiResponse("updated successfully", true);
    }

    public ApiResponse deleteCustomer(Integer id) {
        if (!customerRepository.existsById(id))
            return new ApiResponse("customer doesn't exists", false);

        customerRepository.deleteById(id);

        return new ApiResponse("deleted successfully", true);
    }
}
