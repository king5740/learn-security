package com.example.learnsecurity.repository;

import com.example.learnsecurity.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<User, Integer> {
    boolean existsByPhoneNumber(String number);
    boolean existsByPhoneNumberAndIdNot(String phoneNumber, Integer id);

    }
